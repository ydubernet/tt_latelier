using Moq;
using System.Collections.Generic;
using TennisPlayers.Models;
using TennisPlayers.Repos;
using TennisPlayers.Services;
using Xunit;

namespace TennisPlayers.Tests
{
    public class StatisticsServiceTests
    {
        [Fact]
        public void Test_EnsureStatisticsBehavesCorrectly()
        {
            var mockRepo = new Mock<IStatisticsRepository>();
            mockRepo.Setup(r => r.GetStatistics())
                .Returns(new List<Statistics>()
                {
                    new Statistics()
                    {
                        FirstName = "Yoann",
                        LastName = "Dubernet",
                        Data = new Data()
                        {
                            Age = 29,
                            Height = 180,
                            Last = new int[] { 1, 2, 3, 4,},
                            Points = 42,
                            Rank = 42,
                            Weight = 65
                        }
                    }

                });

            var statisticsService = new StatisticsService(mockRepo.Object);

            var stats = statisticsService.GetStatistics();
            Assert.NotNull(stats);
            Assert.NotEmpty(stats);
        }
    }
}