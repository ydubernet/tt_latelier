﻿using Microsoft.Extensions.Logging;
using Moq;
using TennisPlayers.Repos;
using Xunit;

namespace TennisPlayers.Tests
{
    public class StatisticsRepositoryTests
    {
        [Fact]
        public void Test_StatisticsRepositoryLoadsCorrectly()
        {
            var repo = new StatisticsRepository(Mock.Of<ILogger<StatisticsRepository>>());
            var stats = repo.GetStatistics();
            Assert.NotNull(stats);
        }

    }
}
