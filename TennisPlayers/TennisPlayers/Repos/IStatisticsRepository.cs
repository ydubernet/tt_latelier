﻿using TennisPlayers.Models;

namespace TennisPlayers.Repos
{
    public interface IStatisticsRepository
    {
        IList<Statistics> GetStatistics();
    }
}
