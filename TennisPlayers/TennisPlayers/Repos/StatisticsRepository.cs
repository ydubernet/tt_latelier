﻿// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using TennisPlayers.Models;

namespace TennisPlayers.Repos
{
    public class StatisticsRepository : IStatisticsRepository
    {
        private const string PATH_TO_DATASET = "./Repos/Data/headtohead.json";
        private readonly ILogger<StatisticsRepository> _logger;
        private List<Statistics> _statistics;

        public StatisticsRepository(ILogger<StatisticsRepository> logger)
        {
            _logger = logger;
            _statistics = new List<Statistics>();

            var timer = new Timer(RefreshStatistics, null, 0, 10000); // Auto refresh once every 10 seconds
        }

        private void RefreshStatistics(object? state)
        {
            try
            {
                _statistics = JsonConvert.DeserializeObject<List<Statistics>>(new StreamReader(PATH_TO_DATASET).ReadToEnd(), new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver()});
            }
            catch (Exception ex)
            {
                _logger.LogError("Error while deserializing dataset:" + ex.Message);
            }
        }

        public IList<Statistics> GetStatistics() {
            if (_statistics.Count == 0) RefreshStatistics(null); // Force refresh (shitty hack but not really the time)
            return _statistics;
        }
    }
}
