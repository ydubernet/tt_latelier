﻿namespace TennisPlayers.Models
{
    public class Statistics
    {
        public string FirstName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public Data Data { get; set; } = default;
    }

    public struct Data
    {
        public int Rank { get; set; }
        public int Points { get; set; }
        public int Weight { get; set; } //(in grams)
        public int Height { get; set; }
        public int Age { get; set; }

        public int[] Last { get; set; }

        //"rank": 2,
        //"points": 2542,
        //"weight": 80000,
        //"height": 188,
        //"age": 31,
        //"last": [ 1, 1, 1, 1, 1 ]
    }
}
