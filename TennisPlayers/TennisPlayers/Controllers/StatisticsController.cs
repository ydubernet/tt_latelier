﻿using Microsoft.AspNetCore.Mvc;
using TennisPlayers.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TennisPlayers.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatisticsController : ControllerBase
    {
        private readonly IStatisticsService _statisticsService;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="statisticsService"></param>
        public StatisticsController(IStatisticsService statisticsService)
        {
            _statisticsService = statisticsService;
        }

        // GET: api/<ValuesController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            var allStatistics = _statisticsService.GetStatistics();
            return new List<string>(); // TODO
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            //return _statisticsService.GetStatistics(id);
            return "bla";
        }
    }
}
