﻿using TennisPlayers.Models;

namespace TennisPlayers.Services
{
    public interface IStatisticsService
    {
        IList<Statistics> GetStatistics();
        Statistics GetStatistic(int id);
    }
}
