﻿using TennisPlayers.Models;
using TennisPlayers.Repos;

namespace TennisPlayers.Services
{
    public class StatisticsService : IStatisticsService
    {
        private readonly IStatisticsRepository _statisticsRepository;

        public StatisticsService(IStatisticsRepository statisticsRepository)
        {
            _statisticsRepository = statisticsRepository;
        }
      

        public IList<Statistics> GetStatistics() => _statisticsRepository.GetStatistics();

        public Statistics GetStatistic(int id) => _statisticsRepository.GetStatistics()[id];

    }
}
